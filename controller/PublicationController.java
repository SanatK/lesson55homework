package kg.attractor.microgram2.controller;
import kg.attractor.microgram2.dto.CommentDTO;
import kg.attractor.microgram2.dto.LikeDTO;
import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.dto.SubscribesDTO;
import kg.attractor.microgram2.model.Comment;
import kg.attractor.microgram2.model.User;
import kg.attractor.microgram2.service.PublicationService;
import kg.attractor.microgram2.service.SubscribesService;
import kg.attractor.microgram2.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class PublicationController {
    private final PublicationService publicationService;
    private final UserService userService;
    private final SubscribesService subscriptionService;


    @PostMapping("/publication")
    public PublicationDTO addPublication(Authentication authentication, @RequestBody PublicationDTO publicationData) {
        User user = (User) authentication.getPrincipal();
        return publicationService.addPublication(publicationData, user.getId());
    }
    @PostMapping("/comment")
    public String addComment(Authentication authentication, @RequestBody CommentDTO commentData){
        User user = (User) authentication.getPrincipal();
        return publicationService.addComment(commentData, user.getId());
    }
    @DeleteMapping("comment/{commentId}")
    public ResponseEntity<Void> deleteComment(Authentication authentication, @PathVariable String commentId) {
        User user = (User) authentication.getPrincipal();
        if (publicationService.deleteComment(user.getId(), commentId))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<Void> deleteUser(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        if (userService.deleteUser(user.getId()))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/publication/{publicationId}")
    public ResponseEntity<Void> deletePublication(Authentication authentication, @PathVariable String publicationId) {
        User user = (User) authentication.getPrincipal();
        if (publicationService.deletePublication(publicationId, user.getId()))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/subscribe")
    public SubscribesDTO subscribeOnUser(Authentication authentication, @RequestBody SubscribesDTO subscriptionData){
        User user = (User) authentication.getPrincipal();
        return subscriptionService.createSubscription(user.getId(), subscriptionData);
    }
    @PostMapping("/publication/like")
    public String likePublication(Authentication authentication, @RequestBody LikeDTO likeData) {
         User user = (User) authentication.getPrincipal();
         return publicationService.likePublication(user.getId(), likeData);
    }
}
