package kg.attractor.microgram2.util;
import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.UserRepository;
import org.springframework.context.annotation.Configuration;



@Configuration
public class PreloadDatabaseWithData {

public final UserRepository ur;
public final CommentRepository cr;


public  PreloadDatabaseWithData(UserRepository ur, CommentRepository cr){
    this.ur = ur;
    this.cr = cr;
}
}
