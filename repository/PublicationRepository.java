package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PublicationRepository extends CrudRepository<Publication, String> {
    public void deleteById(String id);
    List<Publication> findAllByUserId(String userId);
}
