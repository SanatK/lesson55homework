package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Like;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepository extends CrudRepository<Like, String> {
    List<Like> findAllByPublicationId(String publicationId);
}
