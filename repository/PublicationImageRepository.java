package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.PublicationImage;
import org.springframework.data.repository.CrudRepository;

public interface PublicationImageRepository extends CrudRepository<PublicationImage, String> {
}
