package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, String> {
    List<Comment> findAllByPublicationId(String publicationId);
    List<Comment> findAllByUserId(String userId);
}
