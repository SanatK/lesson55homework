package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    public User findByName(String name);
    Optional<User> getByEmail(String email);
}
