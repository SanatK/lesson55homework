package kg.attractor.microgram2.service;

import kg.attractor.microgram2.dto.CommentDTO;
import kg.attractor.microgram2.dto.LikeDTO;
import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.exception.ResourceNotFoundException;
import kg.attractor.microgram2.model.Comment;
import kg.attractor.microgram2.model.Like;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.repository.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PublicationService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    private final PublicationImageRepository publicationImageRepository;
    private final LikeRepository likeRepository;

    public PublicationService(UserRepository userRepository, PublicationRepository publicationRepository, CommentRepository commentRepository, PublicationImageRepository publicationImageRepository, LikeRepository likeRepository) {
        this.userRepository = userRepository;
        this.publicationRepository = publicationRepository;
        this.commentRepository = commentRepository;
        this.publicationImageRepository = publicationImageRepository;
        this.likeRepository = likeRepository;
    }
    public PublicationDTO addPublication(PublicationDTO publicationData, String userId){
        var publicationImage = publicationImageRepository.findById(publicationData.getImageId())
                .orElseThrow(() -> new ResourceNotFoundException("Publication Image with " + publicationData.getImageId() + " doesn't exists!"));
        var publication = Publication.builder()
                .id(publicationData.getId())
                .description(publicationData.getDescription())
                .publicationTime(LocalDateTime.now())
                .userId(userId)
                .image(publicationImage)
                .build();
        publicationRepository.save(publication);
        return PublicationDTO.from(publication);
    }
    public boolean deletePublication(String publicationId, String userId) {
        if(publicationRepository.findById(publicationId).get().getUserId().equals(userId)) {
            publicationRepository.deleteById(publicationId);
            commentRepository.deleteAll(commentRepository.findAllByPublicationId(publicationId));
            likeRepository.deleteAll(likeRepository.findAllByPublicationId(publicationId));
            return true;
        }
        return false;
    }
    public String addComment(CommentDTO commentData, String userId){
        if(publicationRepository.existsById(commentData.getPublicationId())){
            var comment = Comment.builder()
                    .id(commentData.getId())
                    .userId(userId)
                    .publicationId(commentData.getPublicationId())
                    .comment(commentData.getComment())
                    .commentTime(LocalDateTime.now())
                    .build();
            commentRepository.save(comment);
            return String.format("id: %s\n publicationId: %s\n, text: %s\n, time: %s",
                    comment.getId(), comment.getPublicationId(), comment.getComment(), comment.getCommentTime());
        }
        return "This publication doen't exist";
    }
    public boolean deleteComment(String userId, String commentId) {
       if(commentRepository.findById(commentId).get().getUserId().equals(userId)){
           commentRepository.delete(commentRepository.findById(commentId).get());
           return true;
       }
       return false;
    }
    public String likePublication(String userId, LikeDTO likeData){
    if(publicationRepository.existsById(likeData.getPublicationId())){
        var like = Like.builder()
                .id(likeData.getId())
                .userId(userId)
                .publicationId(likeData.getPublicationId())
                .likeTime(LocalDateTime.now())
                .build();
       likeRepository.save(like);
        return String.format("id: %s\n publicationId: %s\n, time: %s",
               like.getId(), like.getPublicationId(), like.getLikeTime());
        }
        return "Publication with this Id does not exist";
    }
}
