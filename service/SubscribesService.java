package kg.attractor.microgram2.service;

import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.dto.SubscribesDTO;
import kg.attractor.microgram2.exception.ResourceNotFoundException;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.Subscribes;
import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.PublicationRepository;
import kg.attractor.microgram2.repository.SubscribesRepository;
import kg.attractor.microgram2.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
@AllArgsConstructor
@Service
public class SubscribesService {
    private final SubscribesRepository subscribesRepository;
    public SubscribesDTO createSubscription(String userId, SubscribesDTO subscriptionData){
        var subscription = Subscribes.builder()
                .id(subscriptionData.getId())
                .subscriberId(userId)
                .userId(subscriptionData.getUserId())
                .subscriptionTime(LocalDateTime.now())
                .build();
        subscribesRepository.save(subscription);
        return SubscribesDTO.from(subscription);
    }
}
