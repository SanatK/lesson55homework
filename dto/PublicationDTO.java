
package kg.attractor.microgram2.dto;

import kg.attractor.microgram2.model.Comment;
import kg.attractor.microgram2.model.Like;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.PublicationImage;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDTO {

    public static PublicationDTO from(Publication publication) {
        var publicationImageId = publication.getImage() == null
               ? "-no-image-id"
              : publication.getImage().getId();

        return builder()
                .id(publication.getId())
                .description(publication.getDescription())
                .publicationTime(publication.getPublicationTime())
                .imageId(publicationImageId)
                .build();
    }

    private String id = UUID.randomUUID().toString();
    private String description;
    private LocalDateTime publicationTime;
    private String imageId;
}
