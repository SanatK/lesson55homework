
package kg.attractor.microgram2.dto;

import kg.attractor.microgram2.model.*;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubscribesDTO {

    public static SubscribesDTO from(Subscribes subscription) {
        return builder()
                .id(subscription.getId())
                .subscriberId(subscription.getSubscriberId())
                .userId(subscription.getUserId())
                .subscriptionTime(subscription.getSubscriptionTime())
                .build();
    }

    private String id;
    private String subscriberId;
    private String userId;
    private LocalDateTime subscriptionTime;
}