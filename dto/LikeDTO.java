
package kg.attractor.microgram2.dto;
import kg.attractor.microgram2.model.Like;
import lombok.*;
import java.time.LocalDateTime;


@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class LikeDTO {

    public static LikeDTO from(Like like) {
        return builder()
                .id(like.getId())
                .publicationId(like.getPublicationId())
                .userId(like.getUserId())
                .likeTime(like.getLikeTime())
                .build();
    }

    private String id;
    private String userId;
    private String publicationId;
    private LocalDateTime likeTime;
}
