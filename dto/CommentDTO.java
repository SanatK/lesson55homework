
package kg.attractor.microgram2.dto;

import kg.attractor.microgram2.model.Comment;
import kg.attractor.microgram2.model.Like;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.PublicationImage;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDTO {

    public static CommentDTO from(Comment comment) {
        return builder()
                .id(comment.getId())
                .userId(comment.getUserId())
                .publicationId(comment.getPublicationId())
                .comment(comment.getComment())
                .commentTime(comment.getCommentTime())
                .build();
    }

    private String id;
    private String userId;
    private String publicationId;
    private String comment;
    private LocalDateTime commentTime;
}
